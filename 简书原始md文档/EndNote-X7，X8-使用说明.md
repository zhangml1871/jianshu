> 作者：连玉君 ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn))


### 中文期刊 EndNote 引文格式样式包

-  My_styles.rar [--点击下载--](https://pan.baidu.com/s/1c1IeO2s)

#### 核心资料
- [EndNote X8简明视频教程 (【**R1**】)](http://blog.sciencenet.cn/blog-563557-1074079.html)  |  [--在线观看--](https://www.bilibili.com/video/av16329866/#page=1)
>![](http://upload-images.jianshu.io/upload_images/7692714-f145c4eac4e6cf4f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
- [万震-重庆大学-手把手教你使用EndNote X8](https://pan.baidu.com/s/1geC8OkN) 写得非常好
- [Endnote_X7 实用教程](https://wenku.baidu.com/view/c1a6c83ef5335a8102d220e4.html) 比较详细

#### EndNote 使用要点

- 安装 EndNote 【**R1**】
  - 关闭 Word，安装 `EndNote X8`;
  - 初装后的 EndNote 没有任何文献，一个空的 Libary
  - 可以在 `EN` 左侧`My Groups`菜单中**右击鼠标**，选择`Create Group`，建立多个目录，以便对文献进行分类管理；

- 向 EndNote 中添加参考文献
   - 手动添加
   - 百度学术、谷歌学术和中国期刊网文献条目  
   - 参见【**R1**】，以及 [Endnote 教程之导入文献](http://www.jianshu.com/p/1ace8545b7fc) 
   - `EN` v.s. `百度学术` 配合使用： 
      - 打开百度学术 &rarr; 查找文献 &rarr; 点击`<>引用`插入EndNote (下载文件后直接打开即可插入 EndNote)

- Word 与 EndNote 配合使用 
  - 快捷键
    - `Alt+1` 在 EndNote 和 Word 之间切换；
    - `Alt+2` 在 Word 光标处插入参考文献条目
    - `Ctrl+D` 在 EndNote 中删除选中参考文献条目
    - 在 EndNote 中，双击选中的条目，可以编辑之
  - 禁止自动更新条目
    每次插入参考文献，EndNote 会实时更新，Word 会变得越来越慢。我们可以禁止实时更新，由自己决定更新时机。只需在 Word 中的 **EndNote**第三列菜单条中点击「**Turn Instant Formatting off**」即可。
  - 选择参考文献格式 (Style)  【**R1**】
  - 自定义参考文献风格  参见 【**R1**】
- EndNote 管理建议 【**R1**】
  - 分类管理文献
  - 分享子库
 

### 其他参考资料
- [Endnote 教程之导入文献](http://www.jianshu.com/p/1ace8545b7fc) 介绍了如何从百度学术、谷歌学术、中国期刊网导入文献到 EndNote 中。
- [EndNote 各种常见问题处理](http://www.jianshu.com/p/67300fdc2e8f)
- [Endnote 教程之添加杂志参考文献格式](http://www.jianshu.com/p/9f83112bb59b) 不用下载，已经都在安装包中了，可以将我给的**My_styles.rar** 解压后（[--点击下载My_styles--](https://pan.baidu.com/s/1c1IeO2s)），按照这篇文站的介绍放入指定文件夹，这样就可以使用我定义好的**经济研究**、**管理世界**等中文期刊的参考文献风格了。

>#### 关于我们
- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [【简书-Stata连享会】](http://www.jianshu.com/u/69a30474ef33) 和 [【知乎-连玉君Stata专栏】](https://www.zhihu.com/people/arlionn)。可以在**简书**和**知乎**中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 推文中的相关数据和程序，以及 [Markdown 格式原文](https://gitee.com/arlionn/jianshu) 可以在 [【Stata连享会-码云】](https://gitee.com/arlionn) 中获取。[【Stata连享会-码云】](https://gitee.com/arlionn) 中还放置了诸多 Stata 资源和程序。如 [Stata命令导航](https://gitee.com/arlionn/stata/wikis/Home) ||  [stata-fundamentals](https://gitee.com/arlionn/stata-fundamentals) ||  [Propensity-score-matching-in-stata](https://gitee.com/arlionn/propensity-score-matching-in-stata) || [Stata-Training](https://gitee.com/arlionn/StataTraining) 等。


>#### 联系我们
- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

---
![Stata连享会二维码](http://upload-images.jianshu.io/upload_images/7692714-74e5c548cbac33d0.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")




